using System;
using MousePointerLibrary;
using Xunit;
using Xunit.Abstractions;

namespace HandyCursorTests
{
    public class MovePointerLibraryTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public MovePointerLibraryTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void GetScreenResolutionTest()
        {
            var platformOs = Environment.OSVersion.Platform;
            if (platformOs == PlatformID.Unix)
            {
                var resolution = LinuxMoveCursor.GetScreenResolution();
                _testOutputHelper.WriteLine($"{resolution.width} x {resolution.height}");
            }
            else if (platformOs == PlatformID.Win32NT)
            {
                var resolution = WindowsMoveCursor.GetScreenResolution();
                _testOutputHelper.WriteLine($"{resolution.width} x {resolution.height}");
            }
        }      
        [Fact]
        public void MoveCursorTest()
        {
            var platformOs = Environment.OSVersion.Platform;
            if (platformOs == PlatformID.Unix)
            {
                var result = LinuxMoveCursor.MoveCursor(100, 100);
                _testOutputHelper.WriteLine($"{result.x} x {result.y}");
            }
            else if (platformOs == PlatformID.Win32NT)
            {
                WindowsMoveCursor.MoveCursor(100, 100);
            }
        }
        
        
        [Fact]
        public void MoveCursorHugeValuesTest()
        {
            var platformOs = Environment.OSVersion.Platform;
            if (platformOs == PlatformID.Unix)
            {
                var result = LinuxMoveCursor.MoveCursor(5000, 3000);
                _testOutputHelper.WriteLine($"{result.x} x {result.y}");
            }
            else if (platformOs == PlatformID.Win32NT)
            {
                WindowsMoveCursor.MoveCursor(5000, 3000);
            }
        }
        
        [Fact]
        public void MoveCursorThousandRepeatTest()
        {
            var platformOs = Environment.OSVersion.Platform;
            if (platformOs == PlatformID.Unix)
            {
                for (int i = 0; i < 1000; i++)
                {
                    var result = LinuxMoveCursor.MoveCursor(100+i, 100+i);
                    _testOutputHelper.WriteLine($"{result.x} x {result.y}"); 
                }

            }
            else if (platformOs == PlatformID.Win32NT)
            {
                for (int i = 0; i < 1000; i++)
                {
                    WindowsMoveCursor.MoveCursor(100+i, 100+i);
                }
            }
        }
        
    }
}