using System;
using System.IO;
using HandyCursor.Models;
using HandyCursor.Services;
using HandyCursor.Services.ObjectPosition;
using ImageProcessingLibrary;
using ObjectDetectionLibrary.Services;
using Xunit;
using Xunit.Abstractions;


namespace HandyCursorTests
{
    public class ObjectDetectionTest
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private const float MinScore = 0.5f;
        private int samples = 10;
        private int counter = 0;
        private bool _cameraThreadRun = true;

        const string ModelDir = "/home/kalisz/Repozytoria/HandyCursor/tensorflow-1/base_models_5/export_12.06.21";
        string pbFile = "frozen_inference_graph.pb";

        public ObjectDetectionTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        /*[Fact]
        public void PredictTest()
        {
            string path = "/home/kalisz/2021-07-25-110424.jpg";
            var service = new ObjectDetectionModelService(
                ModelDir,
                pbFile,
                MinScore,
                ""
            );
            
            SharpMat mat = cv2.imread(path, IMREAD_COLOR.IMREAD_COLOR);
            mat = cv2.cvtColor(mat, ColorConversionCodes.COLOR_BGR2RGB);
            Bitmap bitmap = (Bitmap) Image.FromFile(path);
            var task = service.Predict(mat.astype(np.uint8).reshape(new int[] {1, bitmap.Height, bitmap.Width, 3}),
                new Size(bitmap.Width, bitmap.Height));
            var result = task.Result;
            if (result.Count == 0)
            {
                _testOutputHelper.WriteLine("Brak rezultatów!");
                bitmap.Save("recognition.bmp");
                return;
            }
            var bestResultScore = result.Max(x => x.GetScore());
            var bestResult = result.First(x => x.GetScore().Equals(bestResultScore));
            service.drawObjectOnBitmap(bitmap, bestResult.GetRectangleBox(), bestResultScore, "");
            bitmap.Save("recognition.bmp");
            _testOutputHelper.WriteLine(bestResultScore.ToString());
        }
        */
        
        [Fact]
        public void PredictCameraTest()//TODO z wykorzystaniem serwisu
        {
            var preferences = Preferences.GetInstance();
            preferences.NetworkPath = pbFile;
            preferences.ClassNamesPath = "";
            preferences.CameraId = 0;
            preferences.MinScore = MinScore;
            
            var service = new ObjectDetectionModelService(
                ModelDir,
                pbFile,
                MinScore,
                ""
            );//w celu inicjalizacji
            var camera = CameraSingleton.GetCameraInstance();
            CameraPublisher cameraPublisher = new CameraPublisher();
            ObjectPositionPublisher objectPositionPublisher = new ObjectPositionPublisher();

            cameraPublisher.RaiseTakeFrameEvent += objectPositionPublisher.DetectObject;
            objectPositionPublisher.RaiseObjectPositionEvent += GetDetectedObject;
            cameraPublisher.TakeFrames(camera, ref _cameraThreadRun);
        }

        private void GetDetectedObject(object? sender, ObjectPositionResultEventArgs e)
        {
            var result = e.Result;
            if (result.IsDetected)
            {
                File.WriteAllBytes($"{DateTime.Now}-{result.BestScore}.png", result.ObjectRoiMat);
            }

            counter++;
            if (counter > samples) _cameraThreadRun = false;
        }
    }
}