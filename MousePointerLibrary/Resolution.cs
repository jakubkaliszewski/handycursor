using System.Runtime.InteropServices;

namespace MousePointerLibrary
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Resolution
    {
        public int height;
        public int width;
    }
}