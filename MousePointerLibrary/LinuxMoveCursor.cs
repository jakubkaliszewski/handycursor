using System.Runtime.InteropServices;

namespace MousePointerLibrary
{
    public static class LinuxMoveCursor
    {
        [DllImport(@"./bundledLibraries/libcursor-linux.so")]
        public static extern CursorPosition MoveCursor(int x, int y);
        [DllImport(@"./bundledLibraries/libcursor-linux.so")]
        public static extern Resolution GetScreenResolution();

        [DllImport(@"./bundledLibraries/libcursor-linux.so")]
        public static extern CursorPosition CurrentCursorPosition();
    }
}