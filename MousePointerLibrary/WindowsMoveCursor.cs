using System;
using static PInvoke.User32;

namespace MousePointerLibrary
{
    public static class WindowsMoveCursor
    {
        public static CursorPosition MoveCursor(int x, int y)
        {
            var cursorPos = GetCursorPos();
            CursorPosition position;

            if (!SetCursorPos(cursorPos.x + x, cursorPos.y + y))
                Console.WriteLine("Nie ustawiono kursora");
            
            cursorPos = GetCursorPos();
            position.x = cursorPos.x;
            position.y = cursorPos.y;
            return position;
        }

        public static Resolution GetScreenResolution()
        {
            var x = GetSystemMetrics(SystemMetric.SM_CXVIRTUALSCREEN);
            var y = GetSystemMetrics(SystemMetric.SM_CYVIRTUALSCREEN);

            Resolution resolution = new Resolution() {width = x, height = y};
            return resolution;
        }
        
        public static CursorPosition CurrentCursorPosition()
        {
            var cursorPos = GetCursorPos();
            CursorPosition position;
            
            position.x = cursorPos.x;
            position.y = cursorPos.y;
            return position;
        }
    }
}