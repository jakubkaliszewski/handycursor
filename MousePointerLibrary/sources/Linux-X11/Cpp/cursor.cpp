#include "cursor.h"
#include "resolution.h"

#include <X11/Xlib.h>
#include <iostream>
#include <unistd.h>
#include <string>
#include <stdexcept>

CursorPosition CurrentCursorPosition()
{
    Display *display = XOpenDisplay(0);
    int scr = XDefaultScreen(display);
    Window root_window = XRootWindow(display, scr);
    Window window_returned;
    int root_x, root_y;
    int win_x, win_y;
    unsigned int mask_return;

    XQueryPointer(display, root_window, &window_returned,
                  &window_returned, &root_x, &root_y, &win_x, &win_y,
                  &mask_return);

    CursorPosition pos;
    pos.x = root_x;
    pos.y = root_y;

    XFlush(display);
    XCloseDisplay(display);
    
    return pos;
}

CursorPosition MoveCursor(int x, int y)
{
    Display *display = XOpenDisplay(0);
    int scr = XDefaultScreen(display);
    Window root_window = XRootWindow(display, scr);
    Window window_returned;
    int root_x, root_y;
    int win_x, win_y;
    unsigned int mask_return;

    XQueryPointer(display, root_window, &window_returned,
                  &window_returned, &root_x, &root_y, &win_x, &win_y,
                  &mask_return);

    XWarpPointer(display, None, root_window, 0, 0, 0, 0, root_x + x, root_y + y);
    XQueryPointer(display, root_window, &window_returned,
                  &window_returned, &root_x, &root_y, &win_x, &win_y,
                  &mask_return);

    CursorPosition pos;
    pos.x = root_x;
    pos.y = root_y;

    XFlush(display);
    XCloseDisplay(display);
    
    return pos;
}

Resolution GetScreenResolution()
{
    Display *display = XOpenDisplay(0);
    int scr = XDefaultScreen(display);
    Window root_window = XRootWindow(display, scr);
    int height = DisplayHeight(display, scr);
    int width = DisplayWidth(display, scr);

    Resolution res;
    res.height = height;
    res.width = width;

    return res;
}