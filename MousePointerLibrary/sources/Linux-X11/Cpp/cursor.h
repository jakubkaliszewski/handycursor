#include "resolution.h"
#include "cursorPosition.h"
#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#  ifdef MODULE_API_EXPORTS
#    define MODULE_API __declspec(dllexport)
#  else
#    define MODULE_API __declspec(dllimport)
#  endif
#else
#  define MODULE_API
#endif
MODULE_API CursorPosition MoveCursor(int x, int y);
MODULE_API CursorPosition CurrentCursorPosition();
MODULE_API Resolution GetScreenResolution();
#ifdef __cplusplus
}
#endif