using System.Runtime.InteropServices;

namespace MousePointerLibrary
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CursorPosition
    {
        public int x;
        public int y;
    }
}