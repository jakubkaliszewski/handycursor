using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace NumSharp
{
    /// <summary>
    /// Json zawierający listę ID:Nazwa dla mapowania klas używanej sieci neuronowej.
    /// </summary>
    public sealed class LabelItems
    {
        public IList<ClassItem> Labels { get; set; }

        public LabelItems(IList<ClassItem> labels)
        {
            Labels = labels;
        }
        //TODO do zastanowienia się nad filename czy nie na stałą
        public static LabelItems LoadLabelItemFromJson(string filename)
        {
            using var stream = File.OpenRead(filename);
            return JsonSerializer.DeserializeAsync<LabelItems>(stream).Result ?? new LabelItems(new List<ClassItem>());
        }
    }

    /// <summary>
    /// Pojedyncza para Id klasy obiektu -> nazwa obiektu
    /// </summary>
    public sealed class ClassItem
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}