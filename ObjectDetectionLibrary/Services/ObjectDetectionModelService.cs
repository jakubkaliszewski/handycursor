using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using NumSharp;
using ObjectDetectionLibrary.PredictionResult;
using Tensorflow;
using Tensorflow.NumPy;
using static Tensorflow.Binding;

namespace ObjectDetectionLibrary.Services
{
    public class ObjectDetectionModelService
    {
        // To detect redundant calls
        private bool _disposed = false;
        private readonly float _minScore;

        ~ObjectDetectionModelService() => Dispose(false);
        private Session _session; 
        private Graph _graph;
        private readonly string _modelDir;
        private readonly string _pbFile;

        public ObjectDetectionModelService(string modelDir, string pbFile, float minScore, string classDescriptionFile)
        {
            tf.compat.v1.disable_eager_execution();
            _minScore = minScore;
            _modelDir = modelDir;
            _pbFile = pbFile;
            _graph = ImportGraph();
            _session = tf.Session((_graph));
        }

        private void ReadClassNameFile(string filename)
        {
            LabelItems.LoadLabelItemFromJson(filename);
        }

        private NDArray ReadTensorFromImageFile(string file_name)
        {
            var graph = tf.Graph().as_default();
            var file_reader = tf.io.read_file(file_name, "file_reader");
            var decodeJpeg = tf.image.decode_jpeg(file_reader, channels: 3, name: "DecodeJpeg");
            var casted = tf.cast(decodeJpeg, TF_DataType.TF_UINT8);
            var dims_expander = tf.expand_dims(casted, 0);

            using (var sess = tf.Session(graph))
                return sess.run(dims_expander);
        }

        private NDArray ReadTensorFromImageFile(NDArray imageArray)
        {
            var graph = tf.Graph().as_default();
            var casted = tf.cast(imageArray, TF_DataType.TF_UINT8);
            var dims_expander = tf.expand_dims(casted, 0);

            using (var sess = tf.Session(graph))
                return sess.run(dims_expander);
        }

        public Task<IList<IPredictionResult>> Predict(NDArray imageArray, Size imageSize)
        {
            Tensor tensorNum = _graph.OperationByName("num_detections");
            Tensor tensorBoxes = _graph.OperationByName("detection_boxes");
            Tensor tensorScores = _graph.OperationByName("detection_scores");
            Tensor tensorClasses = _graph.OperationByName("detection_classes");
            Tensor imgTensor = _graph.OperationByName("image_tensor");

            Tensor[] outTensorArr = new Tensor[] {tensorNum, tensorBoxes, tensorScores, tensorClasses};
            NDArray[] resultsArray = _session.run(outTensorArr, new FeedItem(imgTensor, imageArray));

            IList<IPredictionResult> results = new List<IPredictionResult>(1);
            var score = resultsArray[2].ToArray<float>()[0];
            Console.WriteLine($"SCORE: {score.ToString()}");
            if (score == 0.0 || score < _minScore)
                return Task.FromResult(results);
            
            var boxArray = resultsArray[1].ToArray<float>();
            var id = resultsArray[3].ToArray<float>()[0];
            float top = boxArray[0] * imageSize.Height;//0
            float left = boxArray[1] * imageSize.Width;//1
            float bottom = boxArray[2] * imageSize.Height;//2
            float right = boxArray[3] * imageSize.Width;//3

            Rectangle rect = new Rectangle()
            {
                X = (int) Math.Round(left, MidpointRounding.AwayFromZero),
                Y = (int) Math.Round(top, MidpointRounding.AwayFromZero),
                Width = (int) Math.Round((right - left), MidpointRounding.AwayFromZero),
                Height = (int) Math.Round((bottom - top), MidpointRounding.AwayFromZero)
            };

            IPredictionResult result = new PredictionResultImpl(rect, score, (int) id, "");
            results.Add(result);

            return Task.FromResult(results);
        }

        public void drawObjectOnBitmap(Bitmap bmp, Rectangle rect, float score, string name)
        {
            using (Graphics graphic = Graphics.FromImage(bmp))
            {
                graphic.SmoothingMode = SmoothingMode.AntiAlias;

                using (Pen pen = new Pen(Color.Red, 2))
                {
                    graphic.DrawRectangle(pen, rect);

                    Point p = new Point(rect.Right + 5, rect.Top + 5);
                    string text = string.Format("{0}:{1}%", name, (int) (score * 100));
                    graphic.DrawString(text, new Font("Verdana", 8), Brushes.Red, p);
                }
            }
        }

        private Graph ImportGraph()
        {
            var graph = new Graph().as_default();
            graph.Import(Path.Join(_modelDir, _pbFile));
            return graph;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _session.Dispose();
                _graph.Dispose();
                _session = null;
                _graph = null;
            }

            _session = null;
            _graph = null;

            _disposed = true;
        }

        private string ModelDir => _modelDir;

        private string PbFile => _pbFile;
    }
}