using System.Drawing;

namespace ObjectDetectionLibrary.PredictionResult
{
    internal class PredictionResultImpl : IPredictionResult
    {
        private readonly Rectangle _box;
        private readonly float _score;
        private readonly int _classId;
        private readonly string _className;
        
        public PredictionResultImpl(Rectangle box, float score, int classId, string className)
        {
            _box = box;
            _score = score;
            _classId = classId;
            _className = className;
        }

        public Rectangle GetRectangleBox()
        {
            return _box;
        }

        public float GetScore()
        {
            return _score;
        }

        public int GetClassId()
        {
            return _classId;
        }

        public string GetClassName()
        {
            return _className;
        }
    }
}
