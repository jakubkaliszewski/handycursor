using System.Drawing;

namespace ObjectDetectionLibrary.PredictionResult
{
    public interface IPredictionResult
    {
        Rectangle GetRectangleBox();
        float GetScore();
        int GetClassId();
        string GetClassName();
    }
}