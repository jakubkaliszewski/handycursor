using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using OpenCvSharp;

namespace ImageProcessingLibrary
{
    public class Camera : IDisposable
    {
        // To detect redundant calls
        private bool _disposed = false;
        private static ReadOnlyCollection<int> _cameras;

        public static ReadOnlyCollection<int> Cameras
        {
            get
            {
                if (_cameras == null)
                    SetCameraIdList();
                return _cameras;
            }
        }

        ~Camera() => Dispose(false);
        private readonly VideoCapture _camera;
        private readonly int _cameraNumber;
        private readonly List<double> _availableFps;
        private int _interval;
        private double _fps;
        public static event EventHandler<CameraEventArgs> CapturedFrameFromCamera;

        /// <summary>
        /// Publisher przechwyconej klatki z kamery do nasłuchiwaczy.
        /// </summary>
        /// <param name="newImage"> Przechwycona macierz Mat.</param>
        protected virtual void OnCapturedFrameFromCamera(Mat newImage)
        {
            CapturedFrameFromCamera?.Invoke(this, new CameraEventArgs() {Image = newImage});
        }

        public Camera(int cameraNumber = 0, int frameHeight = 720, int frameWidth = 1280)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                _camera = VideoCapture.FromCamera(cameraNumber, VideoCaptureAPIs.V4L2);
            else _camera = new VideoCapture(cameraNumber);
            _camera.FrameHeight = frameHeight;
            _camera.FrameWidth = frameWidth;
            _cameraNumber = cameraNumber;
            if (!_camera.IsOpened())
                throw new OpenCvSharpException(String.Format(Strings.CameraCloseException, _cameraNumber));

            _availableFps = SetAvailableFps();
            _fps = _camera.Fps;
            _interval = DeterminateInterval();
            _camera.ConvertRgb = true;
        }

        private int DeterminateInterval()
        {
            return (int) Math.Round(1000.0 / _fps);
        }

        private List<double> SetAvailableFps()
        {
            double maxFps = _camera.Fps;
            var fpsList = new List<double> {maxFps};
            //15,30,60,120

            for (var fps = maxFps; fps >= 15; fps = (int) Math.Round(fps / 2.0))
                fpsList.Add(fps);

            return fpsList;
        }

        public int CameraNumber => _cameraNumber;

        public List<double> AvailableFps => _availableFps;

        public int Interval => _interval;

        public double Fps => _fps;

        /// <summary>
        /// Przechwytuje klatkę obrazu.
        /// </summary>
        public SharpMat TakeFrame()
        {
            return new SharpMat(_camera.RetrieveMat());
        }

        /// <summary>
        /// Przechwytuje klatkę obrazu jako tablica byte.
        /// </summary>
        public byte[] TakeFrameAsBytes()
        {
            return _camera.RetrieveMat().ToBytes(".bmp"); //Przechwycenie klatki
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _camera.Release();
            }

            _disposed = true;
        }

        private static void SetCameraIdList()
        {
            List<int> cameraIds = new List<int>();
            for (var i = 0; i < 10; i++)
            {
                var cam = new VideoCapture(i);
                if (cam.IsOpened())
                    cameraIds.Add(i);
                cam.Release();
            }

            _cameras = new ReadOnlyCollection<int>(cameraIds);
        }
    }

    public class CameraEventArgs : EventArgs
    {
        public Mat Image { get; set; }
    }
}