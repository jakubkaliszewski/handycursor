using System;
using System.Threading;
using OpenCvSharp;

namespace ImageProcessingLibrary
{
    public class CameraPublisher
    {
        // Declare the event using EventHandler<T>
        public event EventHandler<CameraFrameEventArgs> RaiseTakeFrameEvent;

        public void TakeFrames(Camera camera, ref bool cameraThreadRun)
        {
            while (true)
            {
                SharpMat image = camera.TakeFrame();
                if (image == null) continue;
                //image = CameraMat.ImproveMat(image);
                // Write some code that does something useful here
                // then raise the event. You can also raise an event
                // before you execute a block of code.
                OnRaiseCameraFrameEventArgs(new CameraFrameEventArgs(image));
                if(cameraThreadRun)
                    Thread.Sleep(camera.Interval);
                else return;
            }
        }

        // Wrap event invocations inside a protected virtual method
        // to allow derived classes to override the event invocation behavior
        protected virtual void OnRaiseCameraFrameEventArgs(CameraFrameEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<CameraFrameEventArgs> raiseEvent = RaiseTakeFrameEvent;

            // Call to raise the event.
            raiseEvent?.Invoke(this, e);
        }
    }
}