using System;
using System.Runtime.InteropServices;
using OpenCvSharp;

namespace ImageProcessingLibrary.NativeAPI
{
    internal class cv2_native_api
    {
        const string OpenCvDllName = "OpenCvSharpExtern";
        
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void core_Mat_dims(IntPtr mat, out int output);
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void core_Mat_channels(IntPtr mat, out int output);
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void core_Mat_type(IntPtr mat, out SharpMatType output);
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void core_Mat_sizeAt(IntPtr mat, int dim, out int output);
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern unsafe void core_Mat_data(IntPtr mat, out byte* output);
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern unsafe void core_Mat_data(IntPtr mat, out int* output);
        [DllImport(OpenCvDllName, CallingConvention = CallingConvention.Cdecl)]
        internal static extern unsafe void core_Mat_data(IntPtr mat, out float* output);
    }
}