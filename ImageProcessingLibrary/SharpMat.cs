using System;
using ImageProcessingLibrary.NativeAPI;
using OpenCvSharp;
using Tensorflow;
using Tensorflow.NumPy;

namespace ImageProcessingLibrary
{
    public class SharpMat : Mat
    {
        public SharpMat()
        {}
        public SharpMat(Mat mat) : base(mat)
        {}
        NDArray _nd;
        public NDArray data
        {
            get
            {
                if (_nd is null)
                    _nd = GetData();

                return _nd;
            }
        }

        int _ndim;
        public int ndim
        {
            get
            {
                cv2_native_api.core_Mat_dims(ptr, out _ndim);
                return _ndim + (Channels() > 1 ? 1 : 0);
            }
        }
        
        /*
        int _channels = -1;
        public int Channels
        {
            get
            {
                if (_channels > -1)
                    return _channels;

                cv2_native_api.core_Mat_channels(ptr, out _channels);
                return _channels;
            }
        }*/
        
        public TF_DataType dtype { get; set; } = np.@byte;

        private SharpMatType _matType = SharpMatType.Unknown;
        public SharpMatType MatType
        {
            get
            {
                if (_matType != SharpMatType.Unknown)
                    return _matType;

                cv2_native_api.core_Mat_type(ptr, out _matType);
                return _matType;
            }
        }
        
        int[] _dims;
        Shape _shape;
        /// <summary>
        /// HWC
        /// height * width * channel
        /// </summary>
        public Shape shape
        {
            get
            {
                if(_dims != null)
                    return _shape;

                _dims = new int[ndim];
                cv2_native_api.core_Mat_dims(ptr, out _ndim);
                
                for (int i = 0; i < _ndim; i++)
                    cv2_native_api.core_Mat_sizeAt(ptr, i, out _dims[i]);
                int channels = Channels();
                if(channels > 1)
                    _dims[_ndim] = channels;

                _shape = new Shape(_dims);
                return _shape;
            }
        }

        public long size => shape.size;
        
        public NDArray astype(TF_DataType dtype)
            => data.astype(dtype);

        public NDArray astype<T>()
            => data.astype(typeof(T).as_tf_dtype());
        
        public unsafe NDArray GetData()
        {
            // we pass donothing as it keeps reference to src preventing its disposal by GC
            switch (MatType)
            {
                case SharpMatType.CV_32SC1:
                case SharpMatType.CV_32SC2:
                {
                    cv2_native_api.core_Mat_data(ptr, out int* dataPtr);
                    dtype = TF_DataType.TF_INT32;
                    return new NDArray(new IntPtr(dataPtr), shape, dtype);
                }
                case SharpMatType.CV_32FC1:
                {
                    cv2_native_api.core_Mat_data(ptr, out float* dataPtr);
                    dtype = TF_DataType.TF_FLOAT;
                    return new NDArray(new IntPtr(dataPtr), shape, dtype);
                }
                case SharpMatType.CV_8UC1:
                case SharpMatType.CV_8UC3:
                {
                    cv2_native_api.core_Mat_data(ptr, out byte* dataPtr);
                    dtype = TF_DataType.TF_UINT8;
                    return new NDArray(new IntPtr(dataPtr), shape, dtype);
                }
                default:
                    throw new NotImplementedException($"Can't find type: {_matType}");
            }
        }


    }
}