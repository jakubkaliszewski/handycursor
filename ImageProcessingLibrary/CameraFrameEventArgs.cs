using System;
using OpenCvSharp;

namespace ImageProcessingLibrary
{
    public class CameraFrameEventArgs : EventArgs
    {
        public CameraFrameEventArgs(SharpMat frame)
        {
            Frame = frame;
        }

        public SharpMat Frame { get; set; }
    }
}