using OpenCvSharp;

namespace ImageProcessingLibrary.Tracking
{
    public class TrackerResult
    {
        private readonly bool _existOnFrame;
        private readonly Rect _roi;
        public TrackerResult(bool existOnFrame, Rect roi)
        {
            _existOnFrame = existOnFrame;
            _roi = roi;
        }

        public bool ExistOnFrame => _existOnFrame;

        public Rect Roi => _roi;
    }
}