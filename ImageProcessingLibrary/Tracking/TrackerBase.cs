using System;
using OpenCvSharp;

namespace ImageProcessingLibrary.Tracking
{
    /// <summary>
    /// Klasa bazowa dla wszelkich algorytmów śledzenia obiektu
    /// </summary>
    public abstract class TrackerBase : IDisposable
    {
        private readonly Tracker _motionTracker;
        
        protected TrackerBase(Tracker motionTracker, Mat initImage, Rect initRoi)
        {
            _motionTracker = motionTracker;
            motionTracker.Init(initImage, initRoi);
        }

        public TrackerResult Update(Mat image)
        {
            Rect roi = new Rect();
            var hasTracked = _motionTracker.Update(image, ref roi);
            return new TrackerResult(hasTracked, roi);
        }

        public void Dispose()
        {
            _motionTracker?.Dispose();
        }
    }
}