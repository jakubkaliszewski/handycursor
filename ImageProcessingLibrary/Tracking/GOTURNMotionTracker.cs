using OpenCvSharp;
using OpenCvSharp.Tracking;

namespace ImageProcessingLibrary.Tracking
{
    public sealed class GOTURNMotionTracker : TrackerBase
    {
        public GOTURNMotionTracker(TrackerGOTURN motionTracker, Mat initImage, Rect initRoi) : base(motionTracker, initImage, initRoi)
        {
        }
    }
}