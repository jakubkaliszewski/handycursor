using OpenCvSharp;
using OpenCvSharp.Tracking;

namespace ImageProcessingLibrary.Tracking
{
    public sealed class MILMotionTracker : TrackerBase
    {
        public MILMotionTracker(TrackerMIL motionTracker, Mat initImage, Rect initRoi) : base(motionTracker, initImage, initRoi)
        {
        }
    }
}