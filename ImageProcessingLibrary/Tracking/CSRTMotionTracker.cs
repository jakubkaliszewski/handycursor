using OpenCvSharp;
using OpenCvSharp.Tracking;

namespace ImageProcessingLibrary.Tracking
{
    public sealed class CSRTMotionTracker : TrackerBase
    {
        public CSRTMotionTracker(TrackerCSRT motionTracker, Mat initImage, Rect initRoi) : base(motionTracker, initImage, initRoi)
        {
        }
    }
}