using OpenCvSharp;
using OpenCvSharp.Tracking;

namespace ImageProcessingLibrary.Tracking
{
    public sealed class KCFMotionTracker : TrackerBase
    {
        public KCFMotionTracker(TrackerKCF trackerKcf, Mat initImage, Rect initRoi) : base(trackerKcf, initImage, initRoi)
        {
        }
    }
}