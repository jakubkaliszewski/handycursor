import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import random


def xml_to_csv(path):
    rand = random.SystemRandom()
    xml_train_list = []
    xml_test_list = []
    for xml_file in glob.glob(path + '/*.xml'):
        print(xml_file)
        tree = ET.parse(xml_file)
        root = tree.getroot() 
        for member in root.findall('object'):
            try:
                value = (root.find('filename').text,#filename
                    int(root.find('size')[0].text),#width
                    int(root.find('size')[1].text),#height
                    member[0].text,#class
                    int(member[4][0].text),#xmin
                    int(member[4][1].text),#ymin
                    int(member[4][2].text),#xmax
                    int(member[4][3].text)#ymax
                )
            except:
                continue
            if rand.randint(1, 100) % 10 == 0:
                xml_test_list.append(value)
            else: xml_train_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_train_df = pd.DataFrame(xml_train_list, columns=column_name)
    xml_test_df = pd.DataFrame(xml_test_list, columns=column_name)
    return (xml_train_df, xml_test_df) 


def main():
    image_path = os.path.join(os.getcwd(), 'DestinationDataset')
    data = xml_to_csv(image_path)
    data[0].to_csv('hand_labels_train.csv', index=None)
    data[1].to_csv('hand_labels_test.csv', index=None)
    print('Successfully converted xml to csv.')


main()