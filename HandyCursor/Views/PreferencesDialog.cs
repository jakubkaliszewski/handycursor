#nullable enable
using System;
using Gtk;
using HandyCursor.Models;
using HandyCursor.Services;
using HandyCursor.ViewModels;
using UI = Gtk.Builder.ObjectAttribute;

namespace HandyCursor.Views
{
    public class PreferencesDialog : Dialog
    {
#pragma warning disable 649
#pragma warning disable 169
#pragma warning disable 8602
#pragma warning disable 8618
        [UI] private Button _cancelButton;
        [UI] private Button _applyButton;
        [UI] private Label _cameraIdLabel;
        [UI] private Label _frameWidthLabel;
        [UI] private Label _frameHeightLabel;
        [UI] private Label _algorithmLabel;
        [UI] private Label _networkLabel;
        [UI] private Label _classNamesLabel;
        [UI] private Label _minScoreLabel;
        [UI] private Label _shiftVectorBufferSizeLabel;

        [UI] private ComboBox _cameraIdComboBox;
        [UI] private ComboBox _algorithmComboBox;
        [UI] private FileChooserButton _networkFileChooserButton;
        [UI] private FileChooserButton _classNamesPathChooserButton;

        [UI] private SpinButton _frameWidthSpinButton;
        [UI] private SpinButton _frameHeightSpinButton;
        [UI] private SpinButton _minScoreSpinButton;
        [UI] private SpinButton _shiftVectorBufferSizeSpinButton;
        private readonly PreferencesFormViewModel _model;

        public PreferencesDialog() : this(new Builder("PreferencesDialog.glade"))
        {
        }

        private PreferencesDialog(Builder builder) : base(builder.GetRawOwnedObject("PreferencesDialog"))
        {
            builder.Autoconnect(this);
            _model = new PreferencesFormViewModel();
            DeleteEvent += Window_DeleteEvent;
            _cancelButton.Clicked += Window_DeleteEvent;

            /*Translations*/
            Title = Strings.Preferences;
            _cameraIdLabel.Text = Strings.CameraId;
            _frameWidthLabel.Text = Strings.FrameWidth;
            _frameHeightLabel.Text = Strings.FrameHeight;
            _algorithmLabel.Text = Strings.Algorithm;
            _networkLabel.Text = Strings.NeuralNetwork;
            _classNamesLabel.Text = Strings.ClassNames;
            _minScoreLabel.Text = Strings.MinScoreLabel;
            _shiftVectorBufferSizeLabel.Text = Strings.ShiftVectorBufferSize;
            _cancelButton.Label = Strings.Cancel;

            /* Camera combobox */
            _cameraIdComboBox.EntryTextColumn = 0;
            _cameraIdComboBox.Model = _model.CamerasComboModel.Model;

            var crtCameras = new CellRendererText();
            _cameraIdComboBox.PackStart(crtCameras, true);

            _cameraIdComboBox.SetCellDataFunc(crtCameras, new CellLayoutDataFunc(_model.CamerasComboModel.CellLayoutDataFunc));
            _cameraIdComboBox.Active = _model.PreferencesSetting.CameraId;

            /* Algorithms combobox*/
            _algorithmComboBox.EntryTextColumn = 0;
            _algorithmComboBox.Model = _model.AlgorithmsComboModel.Model;

            var crtAlgorithms = new CellRendererText();
            _algorithmComboBox.PackStart(crtAlgorithms, true);

            _algorithmComboBox.SetCellDataFunc(crtAlgorithms, new CellLayoutDataFunc(_model.AlgorithmsComboModel.CellLayoutDataFunc));
            _algorithmComboBox.Active = (int) _model.PreferencesSetting.Algorithm;

            _frameWidthSpinButton.Adjustment = new Adjustment(10, 320, 1920, 10, 5, 0);
            _frameHeightSpinButton.Adjustment = new Adjustment(10, 240, 1080, 10, 5, 0);
            _frameHeightSpinButton.Value = _model.PreferencesSetting.FrameHeight;
            _frameWidthSpinButton.Value = _model.PreferencesSetting.FrameWidth;
            _shiftVectorBufferSizeSpinButton.Adjustment = new Adjustment(5, 1, 10, 1, 1, 0);
            _shiftVectorBufferSizeSpinButton.Value = _model.PreferencesSetting.ShiftVectorBufferSize;
            _minScoreSpinButton.Adjustment = new Adjustment(1, 10, 90, 1, 1, 0);
            _minScoreSpinButton.Value = (int) (_model.PreferencesSetting.MinScore * 100.0f);
            _networkFileChooserButton.SetFilename(_model.PreferencesSetting.NetworkPath);
            _classNamesPathChooserButton.SetFilename(_model.PreferencesSetting.ClassNamesPath);
            
            _applyButton.Clicked += ApplyButton_Clicked;
        }
#pragma warning restore 649
#pragma warning restore 169
#pragma warning restore 8602
#pragma warning restore 8618

        private void ApplyButton_Clicked(object? sender, EventArgs a){
            var preferences = _model.PreferencesSetting;
            preferences.FrameHeight = _frameHeightSpinButton.ValueAsInt;
            preferences.FrameWidth = _frameWidthSpinButton.ValueAsInt;
            preferences.Algorithm = (MotionTrackingAlgorithmEnum) _algorithmComboBox.Active;
            preferences.CameraId = _cameraIdComboBox.Active;
            preferences.NetworkPath = _networkFileChooserButton.Filename;
            preferences.ClassNamesPath = _classNamesPathChooserButton.Filename;
            preferences.MinScore = _minScoreSpinButton.ValueAsInt / 100f;
            preferences.ShiftVectorBufferSize = _shiftVectorBufferSizeSpinButton.ValueAsInt;
            PreferencesService.SavePreferences(preferences);

            CameraSingleton.SetCameraInstance(preferences.CameraId, preferences.FrameHeight, preferences.FrameWidth);
            
            Dispose();
        }

        private void Window_DeleteEvent(object? sender, EventArgs e)
        {
            Dispose();
        }
    }
}