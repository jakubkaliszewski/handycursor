#nullable enable
using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace HandyCursor.Views
{
    public class OwnAboutDialog : AboutDialog
    {
#pragma warning disable 649
#pragma warning disable 169
#pragma warning disable 8602
#pragma warning disable 8618
        [UI] private Button _closeAboutButton;
        public OwnAboutDialog() : this(new Builder("OwnAboutDialog.glade"))
        {
        }

        private OwnAboutDialog(Builder builder) : base(builder.GetRawOwnedObject("OwnAboutDialog"))
        {
            builder.Autoconnect(this);
            DeleteEvent += Window_DeleteEvent;
            _closeAboutButton.Clicked += Window_DeleteEvent;
            Comments = Strings.CommentsAbout;
        }
        
#pragma warning restore 649
#pragma warning restore 169
#pragma warning restore 8602
#pragma warning restore 8618
        
        private void Window_DeleteEvent(object? sender, EventArgs e)
        {
            Dispose();
        }
    }
}