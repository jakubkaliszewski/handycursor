#nullable enable
using System;
using System.Threading;
using Gdk;
using Gtk;
using HandyCursor.Services;
using HandyCursor.Services.MoveCursor;
using HandyCursor.Services.ObjectPosition;
using HandyCursor.Services.ShiftVector;
using ImageProcessingLibrary;
using Application = Gtk.Application;
using Image = Gtk.Image;
using Thread = System.Threading.Thread;
using UI = Gtk.Builder.ObjectAttribute;
using Window = Gtk.Window;

namespace HandyCursor
{
    class MainWindow : Window
    {
#pragma warning disable 649
#pragma warning disable 169
#pragma warning disable 8602
#pragma warning disable 8618

        [UI] private ImageMenuItem _preferencesItem;
        [UI] private ImageMenuItem _quitItem;
        [UI] private ImageMenuItem _aboutItem;

        [UI] private Button _startButton;

        [UI] private volatile Image _recognitionImage;
        [UI] private volatile Image _rawImage;

        [UI] private Label _statisticsLabel;
        [UI] private Label _modeLabel;
        [UI] private Label _modeValueLabel;
        [UI] private Label _probabilityLabel;
        [UI] private Label _probabilityValueLabel;
        [UI] private Label _trackingAlgorithmLabel;
        [UI] private Label _trackingAlgorithmValueLabel;
        [UI] private Label _cursorLabel;
        [UI] private Label _cursorValueLabel;
        [UI] private Label _moveValueLabel;
        [UI] private Label _moveLabel;
        private volatile Thread? _cameraThread;
        private bool _cameraThreadRun;

        public MainWindow() : this(new Builder("MainWindow.glade"))
        {
        }

        private MainWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            _cameraThread = null;
            builder.Autoconnect(this);
            ValidateCameras();
            
            /*Translations*/
            _preferencesItem.Label = Strings.Preferences;
            _quitItem.Label = Strings.Quit;
            _aboutItem.Label = Strings.About;

            _startButton.Label = Strings.StartButton;
            _statisticsLabel.Text = Strings.Statistics;
            _modeLabel.Text = Strings.ModeLabel;
            _probabilityLabel.Text = Strings.ProbabilityLabel;
            _trackingAlgorithmLabel.Text = Strings.TrackingAlgorithmLabel;
            _cursorLabel.Text = Strings.CursorLabel;
            _moveLabel.Text = Strings.MoveLabel;

            /*Events*/
            DeleteEvent += Window_DeleteEvent;
            _quitItem.Activated += Window_DeleteEvent;
            _aboutItem.Activated += AboutItemOnActivated;
            _preferencesItem.Activated += PreferencesItemOnActivated;
            _startButton.Clicked += StartButton_Clicked;
        }

#pragma warning restore 649
#pragma warning restore 169
#pragma warning restore 8602
#pragma warning restore 8618

        private void ValidateCameras()
        {
            if (Camera.Cameras.Count == 0)
            {
                var errorDialog = new MessageDialog(
                    this,
                    DialogFlags.Modal,
                    MessageType.Error,
                    ButtonsType.Close,
                    Strings.NoCamera
                );
                errorDialog.Run();
                errorDialog.Dispose();
                Environment.Exit(1);
            }
        }

        private void AboutItemOnActivated(object? sender, EventArgs e)
        {
            var aboutDialog = new Views.OwnAboutDialog();
            aboutDialog.Show();
        }

        private void PreferencesItemOnActivated(object? sender, EventArgs e)
        {
            var preferencesDialog = new Views.PreferencesDialog();
            preferencesDialog.Show();
        }

        private void Window_DeleteEvent(object? sender, EventArgs e)
        {
            if (_cameraThreadRun)
            {
                _cameraThreadRun = false;
                _cameraThread.Join();
            }

            Application.Quit();
        }

        private void StartButton_Clicked(object? sender, EventArgs a)
        {
            if (_cameraThread != null && _cameraThreadRun)
            {
                OnCameraThreadStop();
            }
            else
            {
                _cameraThread = new Thread(new ThreadStart(CameraThreadRoutine));
                _cameraThread.Priority = ThreadPriority.Highest;
                _cameraThreadRun = true;
                _cameraThread.Start();

                _startButton.Label = Strings.StopButton;
                _aboutItem.Sensitive = false;
                _quitItem.Sensitive = false;
                _preferencesItem.Sensitive = false;
            }
        }

        void CameraThreadRoutine()
        {
            try
            {
                var objectPositionService =
                    ObjectPositionService.GetInstance(); //celowe - w celu przygotowania instancji
            }
            catch (Exception e)
            {
                Gtk.Application.Invoke(delegate
                {
                    var errorDialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Close,
                        e.Message);
                    errorDialog.Run();
                    errorDialog.Dispose();
                    OnCameraThreadStop();
                });
                return;
            }

            var camera = CameraSingleton.GetCameraInstance();
            CameraPublisher cameraPublisher = new CameraPublisher();
            ObjectPositionPublisher objectPositionPublisher = new ObjectPositionPublisher();
            ShiftVectorPublisher shiftVectorPublisher = new ShiftVectorPublisher();
            MoveCursorPublisher moveCursorPublisher = new MoveCursorPublisher();

            cameraPublisher.RaiseTakeFrameEvent += objectPositionPublisher.DetectObject;
            objectPositionPublisher.RaiseObjectPositionEvent += shiftVectorPublisher.ComputeShiftVector;
            objectPositionPublisher.RaiseObjectPositionEvent += UpdatePreviews;
            shiftVectorPublisher.RaiseComputeShiftVectorEvent += moveCursorPublisher.MoveCursor;
            moveCursorPublisher.RaiseMoveCursorEvent += UpdateStatistics;
            cameraPublisher.TakeFrames(camera, ref _cameraThreadRun);
        }

        private void OnCameraThreadStop()
        {
            _cameraThreadRun = false;
            _cameraThread.Join();
            _cameraThread = null;
            Gtk.Application.Invoke(delegate
            {
                _startButton.Label = Strings.StartButton;
                _cursorValueLabel.Text = "";
                _modeValueLabel.Text = "";
                _moveValueLabel.Text = "";
                _probabilityValueLabel.Text = "";
                _trackingAlgorithmValueLabel.Text = "";
                _aboutItem.Sensitive = true;
                _quitItem.Sensitive = true;
                _preferencesItem.Sensitive = true;
            });

            try
            {
                ObjectPositionService.GetInstance().Dispose();
            }catch(Exception e){}
            GC.Collect();
        }
        
        private void UpdatePreviews(object? sender, ObjectPositionResultEventArgs objectPositionResultEventArgs)
        {
            Gtk.Application.Invoke(delegate
            {
                try
                {
                    UpdateImagePreview(objectPositionResultEventArgs.Result.OriginalMat, _rawImage);
                    UpdateImagePreview(objectPositionResultEventArgs.Result.ObjectRoiMat, _recognitionImage);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            });
        }

        private void UpdateImagePreview(byte[] matAsBytes, Image image)
        {
            PixbufLoader loader = new ();
            loader.Write(matAsBytes);
            loader.SetSize(320, 240);
            image.Pixbuf = loader.Pixbuf;
            loader.Close();
        }

        private void UpdateStatistics(object? sender, MoveCursorResultEventArgs moveCursorResultEventArgs)
        {
            var result = moveCursorResultEventArgs.Result;
            Gtk.Application.Invoke(delegate
            {
                try
                {
                    _cursorValueLabel.Text = result.CursorPosition.ToString();
                    _modeValueLabel.Text = result.OperatingMode ?? "";
                    _moveValueLabel.Text = result.ShiftVector.ToString();
                    if (!string.IsNullOrEmpty(result.BestScore))
                        _probabilityValueLabel.Text = result.BestScore;
                    _trackingAlgorithmValueLabel.Text = result.TrackingAlgorithm ?? "";
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            });
        }
    }
}