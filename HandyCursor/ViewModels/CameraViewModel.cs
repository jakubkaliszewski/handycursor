namespace HandyCursor.ViewModels
{
    public class CameraViewModel : IListable
    {
        private int CameraId { get; }
        private string Name { get; set; }

        public int Key => CameraId;
        public string Description => Name;
        public IListable Record => this;

        public CameraViewModel(int cameraId, string cameraName)
        {
            CameraId = cameraId;
            Name = cameraName;
        }

        public bool Equals(IListable other)
        {
            return other != null && Key == other.Key;
        }
    }
}