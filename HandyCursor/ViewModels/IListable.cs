using System;

namespace HandyCursor.ViewModels
{
    public interface IListable : IEquatable<IListable>
    {
        int       Key         { get; }
        String    Description { get; }
        IListable Record      { get; }
    }
}