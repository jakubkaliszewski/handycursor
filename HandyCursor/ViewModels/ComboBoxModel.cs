using System.Collections.Generic;
using Gtk;

namespace HandyCursor.ViewModels
{
    public sealed class ComboBoxModel
    {
        private ListStore _store;

        public ListStore Model => _store;

        public ComboBoxModel(List<IListable> items)
        {
            InitializeDetailList(items);
        }

        public void CellLayoutDataFunc(ICellLayout column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            IListable dtl = (IListable) model.GetValue(iter, 0);
            if (dtl == null)
                return;

            ((CellRendererText) cell).Text = dtl.Description;
        }

        private void InitializeDetailList(List<IListable> items)
        {
            _store = new ListStore(typeof(IListable));
            if (items == null || items.Count <= 0) return;
            foreach (var item in items)
            {
                _store.AppendValues(item);
            }
        }
    }
}