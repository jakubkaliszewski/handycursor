namespace HandyCursor.ViewModels
{
    public class AlgorithmViewModel : IListable
    {
        private int AlgorithmId { get; }
        private string Name { get; set; }

        public int Key => AlgorithmId;
        public string Description => Name;
        public IListable Record => this;

        public AlgorithmViewModel(int algorithmId, string algorithmName)
        {
            AlgorithmId = algorithmId;
            Name = algorithmName;
        }

        public bool Equals(IListable other)
        {
            return other != null && Key == other.Key;
        }
    }
}