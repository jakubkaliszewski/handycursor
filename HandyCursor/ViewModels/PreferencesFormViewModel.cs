using System;
using System.Collections.Generic;
using HandyCursor.Models;

namespace HandyCursor.ViewModels
{
    public sealed class PreferencesFormViewModel
    {
        public string PreferencesName => Strings.Preferences;
        public string Help => Strings.Help;
        public string File => Strings.File;
        public string About => Strings.About;
        public string Quit => Strings.Quit;

        public Preferences PreferencesSetting => Preferences.GetInstance();
        public ComboBoxModel CamerasComboModel { get; } = new ComboBoxModel(GetSystemCameras());
        public ComboBoxModel AlgorithmsComboModel { get; } = new ComboBoxModel(GetAvailableAlgorithms());
        private static List<IListable> GetSystemCameras()
        {
            List<IListable> cameras = new List<IListable>();
            
            foreach (var camera in ImageProcessingLibrary.Camera.Cameras)
            {
                cameras.Add(new CameraViewModel(camera, camera.ToString()));  
            }

            return cameras;
        } 
        
        private static List<IListable> GetAvailableAlgorithms()
        {
            List<IListable> algorithms = new List<IListable>();

            int id = 0;
            foreach (var algorithm in Enum.GetNames(typeof(MotionTrackingAlgorithmEnum)))
            {
                algorithms.Add(new AlgorithmViewModel(++id, algorithm));  
            }

            return algorithms;
        } 
    }
}