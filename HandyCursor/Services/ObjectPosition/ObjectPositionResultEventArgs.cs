using System;

namespace HandyCursor.Services.ObjectPosition
{
    public class ObjectPositionResultEventArgs : EventArgs
    {
        public ObjectPositionResultEventArgs(ObjectPositionResult result)
        {
            Result = result;
        }

        public ObjectPositionResult Result { get; }
    }
}