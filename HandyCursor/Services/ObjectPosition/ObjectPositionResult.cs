using System;
using System.Drawing;
using System.Globalization;
using ImageProcessingLibrary;
using OpenCvSharp;
using Size = System.Drawing.Size;

namespace HandyCursor.Services.ObjectPosition
{
    public sealed class ObjectPositionResult
    {
        private readonly byte[] _originalMat;
        private readonly byte[] _objectRoiMat;
        private readonly Rectangle _objectRoi;
        private readonly string _bestScore;
        private readonly string _operatingMode;
        private readonly string _trackingAlgorithm;
        private readonly Size _imageSize;

        private static readonly ImageEncodingParam _encodingParam = new ImageEncodingParam(ImwriteFlags.PngCompression, 99);

        /// <summary>
        /// Konstruktor używany gdy obiekt jest poszukiwany
        /// </summary>
        public ObjectPositionResult(Mat originalMat)
        {
            if (originalMat == null || originalMat.Empty() || originalMat.IsDisposed)
                throw new ArgumentException("Mat image is invalid!");
            var bytes = originalMat.ImEncode(prms:_encodingParam);
            _originalMat = bytes;
            _objectRoiMat = bytes;
            _operatingMode = Strings.ObjectSearch;
            _imageSize = new Size(originalMat.Width, originalMat.Height);
        }

        /// <summary>
        /// Konstruktor używany gdy obiekt pozostał znaleziony
        /// </summary>
        public ObjectPositionResult(
            Mat originalMat,
            Mat objectRoiMat,
            Rectangle objectRoi,
            float bestScore,
            int bestClassId,
            string bestClassName
        )
        {
            try
            {
                _originalMat = originalMat.ImEncode(prms:_encodingParam);
                _objectRoiMat = objectRoiMat.ImEncode(prms:_encodingParam);
                _imageSize = new Size(originalMat.Width, originalMat.Height);

                _objectRoi = objectRoi;
                _bestScore = bestScore.ToString(CultureInfo.CurrentCulture);
                _operatingMode = Strings.ObjectFound;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Konstruktor używany gdy obiekt jest aktywnie śledzony
        /// </summary>
        public ObjectPositionResult(
            Mat originalMat,
            Mat objectRoiMat,
            Rectangle objectRoi,
            string trackingAlgorithm
        )
        {
            try
            {
                _originalMat = originalMat.ImEncode(prms:_encodingParam);
                _objectRoiMat = objectRoiMat.ImEncode(prms:_encodingParam);
                _imageSize = new Size(originalMat.Width, originalMat.Height);

                _objectRoi = objectRoi;
                _trackingAlgorithm = trackingAlgorithm;
                _operatingMode = Strings.ObjectIsTracked;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool IsTracked => _trackingAlgorithm != null;
        public bool IsInDetection => !IsTracked && IsDetected;
        public bool IsDetected => _trackingAlgorithm == null && _bestScore != null;

        public byte[] OriginalMat => _originalMat;

        public byte[] ObjectRoiMat => _objectRoiMat;

        public Rectangle ObjectRoi => _objectRoi;

        public string BestScore => _bestScore;

        public string OperatingMode => _operatingMode;

        public string TrackingAlgorithm => _trackingAlgorithm;

        public Size ImageSize => _imageSize;
    }
}