using System;
using ImageProcessingLibrary;

namespace HandyCursor.Services.ObjectPosition
{
    public class ObjectPositionPublisher
    {
        // Declare the event using EventHandler<T>
        public event EventHandler<ObjectPositionResultEventArgs> RaiseObjectPositionEvent;

        public void DetectObject(object sender, CameraFrameEventArgs e)
        {
            try
            {
                var objectPositionService = ObjectPositionService.GetInstance();
                var result = objectPositionService.DetectObject(e.Frame);

                OnRaiseObjectPositionResultEventArgs(new ObjectPositionResultEventArgs(result));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        // Wrap event invocations inside a protected virtual method
        // to allow derived classes to override the event invocation behavior
        protected virtual void OnRaiseObjectPositionResultEventArgs(ObjectPositionResultEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<ObjectPositionResultEventArgs> raiseEvent = RaiseObjectPositionEvent;

            // Call to raise the event.
            raiseEvent?.Invoke(this, e);
        }
    }
}