using System;
using System.Drawing;
using System.IO;
using System.Linq;
using HandyCursor.Models;
using ImageProcessingLibrary;
using ImageProcessingLibrary.Tracking;
using ObjectDetectionLibrary.Services;
using OpenCvSharp;
using Tensorflow.NumPy;
using Size = System.Drawing.Size;

namespace HandyCursor.Services.ObjectPosition
{
    /// <summary>
    /// Serwis będący fasadą
    /// </summary>
    public sealed class ObjectPositionService : IDisposable
    {
        private readonly Preferences _preferences;
        private ObjectDetectionModelService _objectDetectionModelService;
        private TrackerBase _trackerBaseService;
        private readonly String _networkPath;
        private readonly String _classNamesPath;

        private volatile WorkModeEnum _actualWorkMode;
        private volatile bool _isStartTracking;

        private static ObjectPositionService _instance;

        private ObjectPositionService()
        {
            _actualWorkMode = WorkModeEnum.DETECTION;
            _preferences = Preferences.GetInstance();

            _networkPath = _preferences.NetworkPath;
            _classNamesPath = _preferences.ClassNamesPath;

            if (string.IsNullOrEmpty(_networkPath) || string.IsNullOrEmpty(_classNamesPath))
                throw new Exception(Strings.PathMessageError);

            var filename = Path.GetFileName(_networkPath);
            var directoryName = Path.GetDirectoryName(_networkPath);
            _objectDetectionModelService =
                new ObjectDetectionModelService(directoryName!, filename, _preferences.MinScore, _classNamesPath);

            _actualWorkMode = WorkModeEnum.DETECTION;
            _isStartTracking = true;
        }

        public static ObjectPositionService GetInstance()
        {
            var preferences = Preferences.GetInstance();
            var networkPath = preferences.NetworkPath;
            var classNamesPath = preferences.ClassNamesPath;

            if (_instance != null && _instance._networkPath.Equals(networkPath) &&
                _instance._classNamesPath.Equals(classNamesPath))
                return _instance;

            _instance?.Dispose();
            _instance = new ObjectPositionService();
            return _instance;
        }

        private void InitTracker(MotionTrackingAlgorithmEnum algorithmEnum, SharpMat initImage, Rect initRoi)
        {
            _trackerBaseService = MotionTrackingAlgorithmFactory.createTracker(algorithmEnum, initImage, initRoi);
        }

        public ObjectPositionResult DetectObject(SharpMat frame)
        {
            //DETEKCJA
            if (_actualWorkMode == WorkModeEnum.DETECTION)
                return TensorflowDetection(frame);
            return TrackerDetection(frame);
        }

        private ObjectPositionResult TensorflowDetection(SharpMat frame)
        {
            NDArray ndArray = frame.astype(np.uint8).reshape(new int[] {1, frame.Height, frame.Width, 3});
            var resultTask = _objectDetectionModelService.Predict(ndArray, new Size(frame.Width, frame.Height));
            var resultTaskResult = resultTask.Result;
            if (!resultTaskResult.Any()) return new ObjectPositionResult(frame);

            _actualWorkMode = WorkModeEnum.TRACKING;
            var bestResult = resultTaskResult[0];
            var bestResultScore = bestResult.GetScore();

            Rect rectRoi = ConvertRectangle(bestResult.GetRectangleBox());
            InitTracker(_preferences.Algorithm, frame, rectRoi);

            return new ObjectPositionResult(
                frame,
                DrawObject(rectRoi, frame),
                ConvertRectangle(rectRoi),
                bestResultScore,
                bestResult.GetClassId(),
                bestResult.GetClassName()
            );
        }

        private ObjectPositionResult TrackerDetection(SharpMat frame)
        {
            //śledzenie obiektu
            var resultTask = _trackerBaseService.Update(frame);
            //nastąpiła utrata obiektu
            if (!resultTask.ExistOnFrame)
            {
                _isStartTracking = true;
                _actualWorkMode = WorkModeEnum.DETECTION;
                _trackerBaseService.Dispose();
                return new ObjectPositionResult(frame);
            }

            _isStartTracking = false;
            return new ObjectPositionResult(
                frame,
                DrawObject(resultTask.Roi, frame),
                ConvertRectangle(resultTask.Roi),
                Enum.GetName(Preferences.GetInstance().Algorithm)
            );
        }

        private Rect ConvertRectangle(Rectangle rectangle)
        {
            return new Rect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        private Rectangle ConvertRectangle(Rect rectangle)
        {
            return new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        private Mat DrawObject(Rect rect, Mat mat)
        {
            Mat matRoi = new Mat();
            Cv2.CopyTo(mat, matRoi);
            Cv2.Rectangle(matRoi, new Rect(rect.X, rect.Y, rect.Width, rect.Height), Scalar.Red, 2,
                LineTypes.AntiAlias);
            return matRoi;
        }

        public void Dispose()
        {
            _objectDetectionModelService?.Dispose();
            _trackerBaseService?.Dispose();
            _objectDetectionModelService = null;
            _trackerBaseService = null;
            _instance = null;
        }
    }
}