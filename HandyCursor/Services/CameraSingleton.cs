using HandyCursor.Models;
using ImageProcessingLibrary;

namespace HandyCursor.Services
{
    public class CameraSingleton
    {
        private static Camera _camera;

        private CameraSingleton()
        {
        }

        public static Camera GetCameraInstance()
        {
            if (_camera != null) return _camera;
            var preferences = Preferences.GetInstance();
            SetCameraInstance(preferences.CameraId, preferences.FrameHeight, preferences.FrameWidth);

            return _camera;
        }

        public static void SetCameraInstance(int cameraId, int frameHeight, int frameWidth)
        {
            _camera?.Dispose();
            _camera = new Camera(cameraId, frameHeight, frameWidth);
        }
    }
}