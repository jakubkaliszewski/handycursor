using System;
using System.Text.Json;
using HandyCursor.Models;

namespace HandyCursor.Services
{
    public static class PreferencesService
    {
        private static readonly String filename = "settings.json";
        public static void SavePreferences(Preferences preferences)
        {
            using (var storage = System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = storage.OpenFile(filename, System.IO.FileMode.Create))
                {
                    JsonSerializer.Serialize(new Utf8JsonWriter(stream), preferences);
                }
            }
        }

        public static Preferences OpenPreferences()
        {
            using (var storage = System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = storage.OpenFile(filename, System.IO.FileMode.OpenOrCreate))
                {
                    var json = JsonSerializer.DeserializeAsync<Preferences.PreferencesJson>(stream).Result;
                    return Preferences.PreferencesJson.GetInstance(json);
                }
            }
        }
    }
}