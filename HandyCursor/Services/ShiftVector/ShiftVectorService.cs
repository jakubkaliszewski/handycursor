using System;
using System.Drawing;
using System.Numerics;
using HandyCursor.Models;
using MousePointerLibrary;

namespace HandyCursor.Services.ShiftVector
{
    /// <summary>
    /// Serwis wykonujący wszelkie operacje dotyczące wektora przesunięcia obiektu, wektora przesunięcia kursora myszy komputera.
    /// </summary>
    public sealed class ShiftVectorService
    {
        private delegate CursorPosition CurrentCursorPosition();

        private Point _lastCursorPosition;

        private Resolution _screenResolution;

        private CursorPosition _currentCursorPosition;

        private static ShiftVectorService _instance;
        private Rectangle[] _rectanglesRingBuffer;
        private int? _rectangleRingBufferIndexToRemove = null;
        private readonly int _rectangleRingBufferSize;
        private int _rectangleRingBufferCountOfElements;

        public static ShiftVectorService GetInstance()
        {
            return _instance ??= new ShiftVectorService();
        }

        private ShiftVectorService()
        {
            var preferences = Preferences.GetInstance();
            _rectangleRingBufferSize = preferences.ShiftVectorBufferSize;
            _rectanglesRingBuffer = new Rectangle[_rectangleRingBufferSize];

            var platformOs = Environment.OSVersion.Platform;
            if (platformOs == PlatformID.Unix)
            {
                _screenResolution = LinuxMoveCursor.GetScreenResolution();
                _currentCursorPosition = LinuxMoveCursor.CurrentCursorPosition();
            }
            else if (platformOs == PlatformID.Win32NT)
            {
                _screenResolution = WindowsMoveCursor.GetScreenResolution();
                _currentCursorPosition = WindowsMoveCursor.CurrentCursorPosition();
            }
        }

        /// <summary>
        /// Uzyskaj gotowy, zeskalowany względem rozdzielczości ekranu wektor przesunięcia pozycji kursora myszy.
        /// </summary>
        /// <param name="roiRectangle"></param>
        /// <param name="frameSize"></param>
        /// <param name="isFirstTime"></param>
        /// <returns></returns>
        public Vector2 GetScaledShiftVector(Rectangle roiRectangle, Size frameSize, bool isFirstTime)
        {
            var shiftVector = GetShiftVector(roiRectangle, frameSize, isFirstTime);
            return ScaleShiftVectorToScreenResolution(frameSize, shiftVector);
        }

        /// <summary>
        /// Uzyskaj wektor przesunięcia pozycji kursora myszy dla podanego obszaru zainteresowania
        /// </summary>
        /// <param name="roiRectangle"></param>
        /// <param name="frameSize"></param>
        /// <param name="isFirstTime"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private Vector2 GetShiftVector(Rectangle roiRectangle, Size frameSize, bool isFirstTime)
        {
            Point midPoint = GetMidpointFromRectangle(roiRectangle);

            if (isFirstTime)
            {
                SetScaledCurrentCursorPosition(frameSize);
                return new Vector2();
            }

            var vector = new Vector2(
                _lastCursorPosition.X - midPoint.X,
                _lastCursorPosition.Y - midPoint.Y
            );

            _lastCursorPosition = midPoint;
            return vector;
        }

        /// <summary>
        /// Uzyskaj wektor przesunięcia przeskalowany dla rozdzielczości ekranu komputera.
        /// </summary>
        /// <param name="frameSize">Wymiary uzyskiwanej ramki z kamery</param>
        /// <param name="shiftVector">Wektor przesunięcia obowiązujący w ramce z kamery</param>
        /// <returns>Wektor przesunięcia przeskalowany dla rozdzielczości ekranu komputera</returns>
        private Vector2 ScaleShiftVectorToScreenResolution(Size frameSize, Vector2 shiftVector)
        {
            float xSizeRatio = _screenResolution.width / (float) frameSize.Width;
            float ySizeRatio = _screenResolution.height / (float) frameSize.Height;
            return new Vector2(
                shiftVector.X * xSizeRatio,
                shiftVector.Y * ySizeRatio);
        }

        /// <summary>
        /// Uzyskanie punktu będącego środkiem obszaru ROI
        /// </summary>
        /// <param name="roiRectangle"></param>
        /// <returns>Środek obszaru ROI</returns>
        private Point GetMidpointFromRectangle(Rectangle roiRectangle)
        {
            int centX = (int) Math.Round(roiRectangle.X + 0.5 * roiRectangle.Width,
                MidpointRounding.ToPositiveInfinity);
            int centY = (int) Math.Round(roiRectangle.Y + 0.5 * roiRectangle.Height,
                MidpointRounding.ToPositiveInfinity);

            return new Point(centX, centY);
        }

        private void SetScaledCurrentCursorPosition(Size frameSize)
        {
            float xSizeRatio = _screenResolution.width / (float) frameSize.Width;
            float ySizeRatio = _screenResolution.height / (float) frameSize.Height;
            _lastCursorPosition = new Point(
                (int) Math.Round(_currentCursorPosition.x / xSizeRatio, MidpointRounding.ToPositiveInfinity),
                (int) Math.Round(_currentCursorPosition.y / ySizeRatio, MidpointRounding.ToPositiveInfinity)
            );
        }

        /// <summary>
        /// Zwraca środkowy punkt uśrednionego obszaru zainteresowania na podstawie wartości zawartej
        /// w strukturze bufora cyklicznego
        /// </summary>
        /// <param name="lastValue">Ostatnia, najświeższa wartość dodawana do bufora cyklicznego</param>
        /// <returns></returns>
        private Point GetMidPointFromAverageValueRectangleRingBuffer(Rectangle lastValue)
        {
            PutValueToRectangleRingBuffer(lastValue);
            Rectangle rectangleRingBuffer = GetAverageValueFromRectangleRingBuffer();
            return GetMidpointFromRectangle(rectangleRingBuffer);
        }

        /// <summary>
        /// Zwraca uśredniony obszar zainteresowania na podstawie wartości zawartej
        /// w strukturze bufora cyklicznego
        /// </summary>
        /// <returns></returns>
        private Rectangle GetAverageValueFromRectangleRingBuffer()
        {
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            for (int index = 0; index < _rectangleRingBufferCountOfElements; index++)
            {
                var rectangle = _rectanglesRingBuffer[index];
                x += rectangle.X;
                y += rectangle.Y;
                width += rectangle.Width;
                height += rectangle.Height;
            }

            x = (int) Math.Round(x / (float) _rectangleRingBufferCountOfElements, MidpointRounding.ToPositiveInfinity);
            y = (int) Math.Round(y / (float) _rectangleRingBufferCountOfElements, MidpointRounding.ToPositiveInfinity);
            width = (int) Math.Round(width / (float) _rectangleRingBufferCountOfElements, MidpointRounding.ToPositiveInfinity);
            height = (int) Math.Round(height / (float) _rectangleRingBufferCountOfElements, MidpointRounding.ToPositiveInfinity);

            return new Rectangle(
                x,
                y,
                width,
                height
            );
        }

        /// <summary>
        /// Umieszcza nowy element do struktury bufora cyklicznego
        /// </summary>
        /// <param name="lastValue">Wartość do dodania</param>
        private void PutValueToRectangleRingBuffer(Rectangle lastValue)
        {
            //do osobnej metody
            if (_rectangleRingBufferIndexToRemove.HasValue)
            {
                _rectanglesRingBuffer[_rectangleRingBufferIndexToRemove.Value] = lastValue;
                if (_rectangleRingBufferIndexToRemove == _rectangleRingBufferSize - 1)
                    _rectangleRingBufferIndexToRemove = 0;
                else _rectangleRingBufferIndexToRemove++;
            }
            else
            {
                _rectanglesRingBuffer[_rectangleRingBufferCountOfElements] = lastValue;
                _rectangleRingBufferCountOfElements++;
                if (_rectangleRingBufferCountOfElements == _rectangleRingBufferSize)
                    _rectangleRingBufferIndexToRemove = 0;
            }
        }
    }
}