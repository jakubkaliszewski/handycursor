using System;
using HandyCursor.Services.MoveCursor;

namespace HandyCursor.Services.ShiftVector
{
    public class ShiftVectorResultEventArgs : EventArgs
    {
        public ShiftVectorResultEventArgs(ShiftVectorResult result)
        {
            Result = result;
        }

        public ShiftVectorResult Result { get; }
    }
}