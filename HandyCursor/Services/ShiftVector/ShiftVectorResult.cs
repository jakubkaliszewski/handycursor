using System.Numerics;

namespace HandyCursor.Services.ShiftVector
{
    /// <summary>
    /// Rezultat wyznaczania wektora przesunięcia
    /// </summary>
    public sealed class ShiftVectorResult
    {
        /// <summary>
        /// Wyznaczony wektor przesunięcia
        /// </summary>
        private readonly Vector2 _shiftVector;
        /// <summary>
        /// Najlepszy rezultat - propagowany dalej
        /// </summary>
        private readonly string _bestScore;
        /// <summary>
        /// Tryb działania - propagowany dalej
        /// </summary>
        private readonly string _operatingMode;
        /// <summary>
        /// Algorytm śledzenia obiektu - propagowany dalej
        /// </summary>
        private readonly string _trackingAlgorithm;

        public ShiftVectorResult(Vector2 shiftVector, string bestScore, string operatingMode, string trackingAlgorithm)
        {
            _shiftVector = shiftVector;
            _bestScore = bestScore;
            _operatingMode = operatingMode;
            _trackingAlgorithm = trackingAlgorithm;
        }

        public Vector2 ShiftVector => _shiftVector;

        public string BestScore => _bestScore;

        public string OperatingMode => _operatingMode;

        public string TrackingAlgorithm => _trackingAlgorithm;
    }
}