using System;
using System.Drawing;
using HandyCursor.Services.ObjectPosition;
using OpenCvSharp;
using Size = System.Drawing.Size;

namespace HandyCursor.Services.ShiftVector
{
    public class ShiftVectorPublisher
    {
        // Declare the event using EventHandler<T>
        public event EventHandler<ShiftVectorResultEventArgs> RaiseComputeShiftVectorEvent;

        public void ComputeShiftVector(object sender, ObjectPositionResultEventArgs e)
        {
            ShiftVectorService shiftVectorService = ShiftVectorService.GetInstance();
            var positionResult = e.Result;
            var scaledShiftVector = shiftVectorService.GetScaledShiftVector(
                positionResult.ObjectRoi,
                new Size(positionResult.ImageSize.Width, positionResult.ImageSize.Height),
                positionResult.IsDetected);
            
            var result = new ShiftVectorResult(
                scaledShiftVector,
                positionResult.BestScore,
                positionResult.OperatingMode,
                positionResult.TrackingAlgorithm
            );
            
            OnRaiseObjectPositionResultEventArgs(new ShiftVectorResultEventArgs(result));
        }

        // Wrap event invocations inside a protected virtual method
        // to allow derived classes to override the event invocation behavior
        protected virtual void OnRaiseObjectPositionResultEventArgs(ShiftVectorResultEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<ShiftVectorResultEventArgs> raiseEvent = RaiseComputeShiftVectorEvent;

            // Call to raise the event.
            raiseEvent?.Invoke(this, e);
        }
    }
}