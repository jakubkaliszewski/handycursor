using System;
using HandyCursor.Services.ObjectPosition;
using HandyCursor.Services.ShiftVector;
using ImageProcessingLibrary;
using OpenCvSharp;

namespace HandyCursor.Services.MoveCursor
{
    public class MoveCursorPublisher
    {
        // Declare the event using EventHandler<T>
        public event EventHandler<MoveCursorResultEventArgs> RaiseMoveCursorEvent;

        public void MoveCursor(object sender, ShiftVectorResultEventArgs e)
        {
            var shiftVectorResult = e.Result;
            var moveCursorService = MoveCursorService.GetInstance();

            var stats = moveCursorService.MoveCursorPointer(shiftVectorResult.ShiftVector);

            var result = new MoveCursorResult(
                stats.ShiftVector,
                stats.CursorPoint,
                shiftVectorResult.BestScore,
                shiftVectorResult.OperatingMode,
                shiftVectorResult.TrackingAlgorithm
            );
            OnRaiseMoveCursorResultEventArgs(new MoveCursorResultEventArgs(result));
        }

        // Wrap event invocations inside a protected virtual method
        // to allow derived classes to override the event invocation behavior
        protected virtual void OnRaiseMoveCursorResultEventArgs(MoveCursorResultEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<MoveCursorResultEventArgs> raiseEvent = RaiseMoveCursorEvent;

            // Call to raise the event.
            raiseEvent?.Invoke(this, e);
        }
    }
}