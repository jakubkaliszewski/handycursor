using System.Drawing;
using System.Numerics;

namespace HandyCursor.Services.MoveCursor
{
    public sealed class MoveCursorResult //TODO Leci do statystyk
    {
        /// <summary>
        /// Wyznaczony wektor przesunięcia
        /// </summary>
        private readonly Vector2 _shiftVector;
        /// <summary>
        /// Pozycja kursora
        /// </summary>
        private readonly Point cursorPosition;
        /// <summary>
        /// Najlepszy rezultat - propagowany dalej
        /// </summary>
        private readonly string _bestScore;
        /// <summary>
        /// Tryb działania - propagowany dalej
        /// </summary>
        private readonly string _operatingMode;
        /// <summary>
        /// Algorytm śledzenia obiektu - propagowany dalej
        /// </summary>
        private readonly string _trackingAlgorithm;

        public MoveCursorResult(Vector2 shiftVector, Point cursorPosition, string bestScore, string operatingMode, string trackingAlgorithm)
        {
            _shiftVector = shiftVector;
            this.cursorPosition = cursorPosition;
            _bestScore = bestScore;
            _operatingMode = operatingMode;
            _trackingAlgorithm = trackingAlgorithm;
        }

        public Vector2 ShiftVector => _shiftVector;

        public Point CursorPosition => cursorPosition;

        public string BestScore => _bestScore;

        public string OperatingMode => _operatingMode;

        public string TrackingAlgorithm => _trackingAlgorithm;
    }
}