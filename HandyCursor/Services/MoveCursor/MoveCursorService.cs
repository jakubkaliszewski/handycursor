using System;
using System.Drawing;
using System.Numerics;
using HandyCursor.Models;
using MousePointerLibrary;

namespace HandyCursor.Services.MoveCursor
{
    /// <summary>
    /// Serwis wykonujący wszelkie operacje dotyczące wektora przesunięcia wskaźnika myszy.
    /// </summary>
    public sealed class MoveCursorService
    {
        private delegate CursorPosition MoveCursor(int x, int y);
        private MoveCursor _moveCursor;
        private static MoveCursorService _instance;

        public static MoveCursorService GetInstance()
        {
            return _instance ??= new MoveCursorService();
        }
        
        private MoveCursorService()
        {
            var platformOs = Environment.OSVersion.Platform;
            if (platformOs == PlatformID.Unix)
            {
                _moveCursor = LinuxMoveCursor.MoveCursor;
            }
            else if (platformOs == PlatformID.Win32NT)
            {
                _moveCursor = WindowsMoveCursor.MoveCursor;
            }
        }

        /// <summary>
        ///Dokonuje przesunięcia pozycji kursora myszy o wyznaczony wektor przesunięcia obiektu 
        /// </summary>
        /// <param name="shiftVector">Wektor przesunięcia, o który ma zostać zmieniona pozycja kursora</param>
        public CursorStats MoveCursorPointer(Vector2 shiftVector)
        {
            CursorPosition cursorPosition = _moveCursor(
                (int) Math.Round(shiftVector.X, MidpointRounding.ToPositiveInfinity),
                (int) Math.Round(shiftVector.Y, MidpointRounding.ToPositiveInfinity)
            );

            return new CursorStats(shiftVector, new Point(cursorPosition.x, cursorPosition.y));
        }
        
    }
}