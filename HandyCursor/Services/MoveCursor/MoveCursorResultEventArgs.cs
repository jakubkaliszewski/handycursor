using System;
using HandyCursor.Services.ObjectPosition;

namespace HandyCursor.Services.MoveCursor
{
    public class MoveCursorResultEventArgs : EventArgs
    {
        public MoveCursorResultEventArgs(MoveCursorResult result)
        {
            Result = result;
        }

        public MoveCursorResult Result { get; }
    }
}