using System;
using HandyCursor.Models;
using ImageProcessingLibrary.Tracking;
using OpenCvSharp;
using OpenCvSharp.Tracking;

namespace HandyCursor.Services
{
    public sealed class MotionTrackingAlgorithmFactory
    {
        public static TrackerBase createTracker(MotionTrackingAlgorithmEnum type, Mat initImage, Rect initRoi)
        {
            switch (type)
            {
                case MotionTrackingAlgorithmEnum.KCF:
                {
                    return new KCFMotionTracker(TrackerKCF.Create(), initImage, initRoi);
                }

                case MotionTrackingAlgorithmEnum.CSRT:
                {
                    return new CSRTMotionTracker(TrackerCSRT.Create(), initImage, initRoi);
                }

                case MotionTrackingAlgorithmEnum.GOTURN:
                {
                    return new GOTURNMotionTracker(TrackerGOTURN.Create(), initImage, initRoi);
                }
                case MotionTrackingAlgorithmEnum.MIL:
                {
                    return new MILMotionTracker(TrackerMIL.Create(), initImage, initRoi);
                }
                default: throw new Exception("Not created motion tracker!");
            }
        }
    }
}