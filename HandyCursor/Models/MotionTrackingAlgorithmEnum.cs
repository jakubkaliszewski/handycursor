namespace HandyCursor.Models
{
    public enum MotionTrackingAlgorithmEnum
    {
        CSRT = 0, //przewidywalne, małe różnice między klatkami
        KCF = 1,
        GOTURN = 2,
        MIL = 3,
    }
}