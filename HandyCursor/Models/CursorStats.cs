using System.Drawing;
using System.Numerics;

namespace HandyCursor.Models
{
    public class CursorStats
    {
        private readonly Vector2 _shiftVector;
        private readonly Point _cursorPoint;

        public CursorStats(Vector2 shiftVector, Point cursorPoint)
        {
            _shiftVector = shiftVector;
            _cursorPoint = cursorPoint;
        }

        public Vector2 ShiftVector => _shiftVector;

        public Point CursorPoint => _cursorPoint;
    }
}