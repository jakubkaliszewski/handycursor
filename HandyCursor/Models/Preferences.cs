using System;
using System.IO;
using HandyCursor.Services;

namespace HandyCursor.Models
{
    public class Preferences
    {
        public int CameraId { get; set; }
        public int FrameWidth { get; set; }
        public int FrameHeight { get; set; }
        public MotionTrackingAlgorithmEnum Algorithm { get; set; }
        public string NetworkPath { get; set; }
        public string ClassNamesPath { get; set; }
        public float MinScore { get; set; }
        public int ShiftVectorBufferSize { get; set; }//TODO formatka do akttualizacji

        private static Preferences _preferences;

        private Preferences()
        {
            CameraId = 0;
            FrameHeight = 240;
            FrameWidth = 320;
            NetworkPath = "";
            ClassNamesPath = "";
            Algorithm = MotionTrackingAlgorithmEnum.KCF;
            MinScore = 0.60f;
            ShiftVectorBufferSize = 5;
        }

        private Preferences(PreferencesJson json)
        {
            CameraId = json.CameraId;
            FrameHeight = json.FrameHeight;
            FrameWidth = json.FrameWidth;
            NetworkPath = json.NetworkPath;
            ClassNamesPath = json.ClassNamesPath;
            Algorithm = json.Algorithm;
            MinScore = json.MinScore;
            ShiftVectorBufferSize = json.ShiftVectorBufferSize;
        }

        public static Preferences GetInstance()
        {
            if (_preferences == null)
            {
                try
                {
                    _preferences = PreferencesService.OpenPreferences();
                }
                catch (Exception e)
                {
                    _preferences = new Preferences();
                    Console.WriteLine(e);
                }
            }

            return _preferences;
        }

        public static bool ValidatePreferences()
        {
            FileAttributes attr = File.GetAttributes(_preferences.NetworkPath);
            return (attr & FileAttributes.Directory) == FileAttributes.Directory
                ? Directory.Exists(_preferences.NetworkPath)
                : File.Exists(_preferences.NetworkPath);
        }

        public class PreferencesJson
        {
            public int CameraId { get; set; }
            public int FrameWidth { get; set; }
            public int FrameHeight { get; set; }
            public MotionTrackingAlgorithmEnum Algorithm { get; set; }
            public string NetworkPath { get; set; }
            public string ClassNamesPath { get; set; }
            public float MinScore { get; set; }
            public int ShiftVectorBufferSize { get; set; }

            public PreferencesJson()
            {
            }

            public static Preferences GetInstance(PreferencesJson json)
            {
                return _preferences ?? (_preferences = new Preferences(json));
            }
        }
    }
}